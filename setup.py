import os
import sys
from setuptools import setup

setup(
    name='api-server',
    author='sgk',
    author_email='sgk@foo.in',
    version='0.0.1',
    include_package_data=True,
    license='BSD License',
    description='An api-server.',
    install_requires=[
        'Flask==0.10.1',
        'Flask-RESTful==0.3.3',
        'SQLAlchemy==1.0.6',
        'Werkzeug==0.10.4',
    ],
    test_suite='tests',
    packages=['app'],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)

#if sys.argv[-1] == 'test':
#    test_requirements = [
#        'Flask==0.10.1',
#        'Flask-RESTful==0.3.3',
#        'SQLAlchemy==1.0.6',
#        'Werkzeug==0.10.4',
#        'app'
#    ]
#    try:
#        modules = map(__import__, test_requirements)
#    except ImportError as e:
#        err_msg = e.message.replace("No module named ", "")
#        msg = "%s is not installed. Install your test requirments." % err_msg
#        raise ImportError(msg)
#    os.system('python tests/*py')
#    sys.exit()
